
// Terraform is used to configure backends where state is stored (metadata)
// NOTE: Assumes bucket exists
terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  backend "s3" {
    bucket = "jfirges-app-bucket"
    key = "jfirges-app/state.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
  shared_credentials_file = var.credentials_file
  profile                 = var.profile_name
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "jfirges-vpc"
  cidr = var.vpc_cidr_block

  azs             = [var.avail_zone]
  public_subnets  = [var.subnet_cidr_block]
  public_subnet_tags = { Name="${var.env_prefix}-subnet-1"}

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

module "jfirges-server" {
  source = "./modules/webserver"
  vpc_id = module.vpc.vpc_id
  my_id = var.my_id
  env_prefix = var.env_prefix
  image_name = var.image_name
  public_key_location = var.public_key_location
  instance_type = var.instance_type
  subnet_id = module.vpc.public_subnets[0]
  avail_zone = var.avail_zone
}
// Open ports 22 and 8080 by creating a security group to allow SSH and web browser access
resource "aws_security_group" "jfirges-sg" {
  vpc_id = var.vpc_id
  name = "jfirges-sg"

  // Incoming traffic - traffic coming inside the VPC (ex: SSH into EC2, access from browser)
  ingress {
    // You can have a range of ports but for now 22 will suffice
    from_port = 22
    to_port = 22
    protocol = "tcp"
    // typically a cidr block is expecting a range of ip addresses, but
    // since we want only one ip address, /32 is used to map it to  
    // the specific ip address below.
    cidr_blocks = [var.my_id]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    // Allow any ip address coming from port 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  // Exiting traffic - any outgoing traffic like fetching docker image or installations
  // Allows traffic to leave the VPC and the server itself 
  egress {
    from_port = 0 // any port
    to_port = 0 // any port
    protocol = "-1" // any protocol
    // Allow any ip address coming from port 8080
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = [] // Allowing to access to any VPC
  }

  tags = {
    Name: "${var.env_prefix}-sg" 
  }
}

// data is used to query information
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners = ["amazon"]
  // filter in data defines criteria for the query. 
  filter {
    name = "name"
    values = [var.image_name]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

// Create server key using our own public/private key
// from .ssh folder in users. If id_rsa files do
// not exist in .ssh create them by using this
// command (ssh-keygen).
resource "aws_key_pair" "ssh-key" {
  key_name = "server-key"
  public_key = file(var.public_key_location)

}

// Create EC2 instance
resource "aws_instance" "jfirges-server" {
  // Need to get the ami(operating system) for the instance.
  // In this case it is AWS linux and it is found when
  // the dashboard under Instances, then Launch Instance.
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  // Optional values. Adding these because VPC is being defined.
  subnet_id = var.subnet_id
  vpc_security_group_ids = [aws_security_group.jfirges-sg.id]
  availability_zone = var.avail_zone

  // Need this so that SSH or a browser can acccess the server
  associate_public_ip_address = true

  // Create key name based on key-pair resource (ssh-key) defined above.  
  // After the apply command completes, as an example, 
  // run ssh ec2-user@52.90.192.43 to ssh into the server.
  key_name = aws_key_pair.ssh-key.key_name

  // Entry point script that gets executed when the EC2 instance gets instantiated.
  // "sudo usermode -aG docker ec2-user" run docker commands without super user(sudo)
  // "ec2-user" - default user on the server.
  // "-aG" - Add to group
  user_data = file("entry-script.sh")
  
  tags = {
    Name: "${var.env_prefix}-server" 
  }
}

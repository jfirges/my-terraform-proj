// attribute name for the public ip. To get a name of an
// attribute, after an apply command completes, type in
// terraform state show aws_instance.myapp-server (name of the server).
// This command will list the attributes.  In this case the public
// ip address that was created when the EC2 instance was created
// will be printed using the output command.
output "ec2_public_ip" {
  value = module.jfirges-server.instance.public_ip
}